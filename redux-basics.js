const redux = require('redux')
const createStore = redux.createStore

const initState = {
    counter: 0
}

// Reducer
const rootReducer = ( state = initState, action) => {
    let copyState = {...state}
    switch (action.type) {
        case 'INC_COUNTER':
            copyState.counter += 1
            break;
        case 'ADD_COUNTER':
            copyState.counter += action.value
            break;
        default:
            break;
    }
    return copyState
}

// Store
const store = createStore(rootReducer)

// Subscription
store.subscribe(() => {
    console.log('[Subscription]')
    console.log(store.getState())
})

// Dispatching Action
store.dispatch({type: 'INC_COUNTER'})
store.dispatch({type: 'ADD_COUNTER', value: 5})